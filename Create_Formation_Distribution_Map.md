Con este script podemos crear el mapa de la distribución de una formación forestal a partir de las capas vectoriales del Mapa Forestal Español a escala 1:50.000 ([MFE50](http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/informacion-disponible/mfe50.aspx)) que hemos podido descargar utilizando este otro [script](https://github.com/ajpelu/MFE50/blob/master/Download_shapefiles_MFE50.md).

En primer lugar establecer el directorio de trabajo y cargar las librerías necesarias. Asimismo vamos a obtener los nombres de los shapefiles de cada provincia (ver [script anterior](https://github.com/ajpelu/MFE50/blob/master/Download_shapefiles_MFE50.md)).

``` r
#---------------------------------------------------------------------------
# Set working directory and load packages
di <- '/Users/ajpelu/myrepos/mapaforestal50'
setwd(paste(di, '/data/shapefiles/', sep=''))

library(maptools)
library(rgdal)
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
# Get the name of the shapefiles
sf <- list.files(pattern = '\\.shp$')
#---------------------------------------------------------------------------
```

A continuación hemos de saber el código de la formación forestal de la cual deseamos obtener el mapa de distribución. Para ello consultaremos el diccionario de datos asociado al MFE50 que puede consultarse en la página del [MFE50](http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/informacion-disponible/mfe50_descargas_ccaa.aspx) (o descargarse en este [enlace](http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_DD_tcm7-192643.xls) directamente). En nuestro caso queremos obtener la distribución del roble melojo (*Quercus pyrenaica* Willd.) que se corresponde con el código 15 en el campo `ID_FORARB`.

``` r
#---------------------------------------------------------------------------
# Select Formation 
# (Quercus pyrenaica = 15) 
formation <- 15 
name_formation <- 'melojar'
#---------------------------------------------------------------------------
```

Seguidamente utilizamos un bucle (`loop`) que realizará los siguientes pasos: \* Leerá los shapefiles (uno por cada provincia) \* Evaluará si la formación está presente en esa provincia \* Seleccionará los polígonos correspondientes a la formación forestal en cada provincia. \* Creará, para cada provincia, un shapefile con el nombre de la formación y el codigo de la provincia. Estos se almacenarán en una carpeta que se creará con el nombre de la formación.

``` r
#---------------------------------------------------------------------------
# Create a shape of the formation by provinces 
for (i in 1:length(sf)){
  # Set working directory 
setwd(paste(di, '/data/shapefiles/', sep='')) 
  
  # Read the shapefile
prov <- readShapePoly(paste(sf[i]))

  # Evaluate if the shape contains formation polygons 
  # If the total row is the same as the no formation rows, then, there aren't formation polygon
total.row <- nrow(prov) 
formation.row <- nrow(prov[prov$ID_FORARB!=formation,])

  if (total.row == formation.row) 
    {  
      next 
    } 
  else { 
  # Select the forest formation 
  shape_formation <- prov[prov$ID_FORARB==formation,]  
  
  # Write the shapefile
  # Name of the destination shapefile
  dest_shapefile <- paste(name_formation, substr(sf[i], start=6, stop=8), sep='')
  ## set wd 
  ## create a directory
  dir.create(paste(di, '/data/shapefiles/',name_formation, sep=''))
  setwd(paste(di, '/data/shapefiles/',name_formation, sep=''))
  ## write shapefile
  writePolyShape(shape_formation, dest_shapefile)
  ## reset wd 
  setwd(paste(di, '/data/shapefiles/', sep='')) 
  }
}
#---------------------------------------------------------------------------
```

Posteriormente los shapefiles provinciales de la formación se combinarán para crear un shapefile de la formación para toda la Península Ibérica, eliminándose los archivos provinciales.

``` r
#---------------------------------------------------------------------------
# Combine the shapefiles of provinces and generate a only shapefile
# see http://stackoverflow.com/questions/5201458/making-a-choropleth-in-r-merging-zip-code-shapefiles-from-multiple-states
setwd(paste(di, '/data/shapefiles/',name_formation, sep=''))

# Get the name of the shapefiles of the formation by provinces 
sf_formation <- list.files(pattern="*.shp$", recursive=TRUE, full.names=TRUE) 

uid <- 1 

# get polygons from first file
poly.data <- readOGR(sf_formation[1], gsub("^.*/(.*).shp$", "\\1", sf_formation[1])) 
n <- length(slot(poly.data, "polygons"))
poly.data <- spChFIDs(poly.data, as.character(uid:(uid+n-1))) 
uid <- uid + n 

# Combine polygons 
for (i in 2:length(sf_formation)) {
  temp.data <- readOGR(sf_formation[i], gsub("^.*/(.*).shp$", "\\1",sf_formation[i]))
  n <- length(slot(temp.data, "polygons")) 
  temp.data <- spChFIDs(temp.data, as.character(uid:(uid+n-1))) 
  uid <- uid + n 
  poly.data <- spRbind(poly.data,temp.data) 
}

# save new shapefile
formation.shp <- paste('formacion',name_formation,'.shp', sep='')
writeOGR(poly.data, dsn=formation.shp, layer=name_formation, driver='ESRI Shapefile')

# Delete files 
for (i in 1:length(sf_formation)){  
  # .shp 
  file.remove(paste(di, '/data/shapefiles/',name_formation, '/',
                    gsub("^.*/(.*)$", "\\1",sf_formation[i]), sep=''))
  # .dbf
  file.remove(paste(di, '/data/shapefiles/',name_formation, '/',
                    gsub("^.*/(.*).shp$", "\\1",sf_formation[i]), '.dbf', sep=''))
  # .shx 
  file.remove(paste(di, '/data/shapefiles/',name_formation, '/',
                    gsub("^.*/(.*).shp$", "\\1",sf_formation[i]), '.shx', sep=''))
}
#---------------------------------------------------------------------------  
```
