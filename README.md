### README

### Mapas de formaciones forestales a partit del Mapa Forestal Español (MFE50)

Este repositorio contiene información para obtener mapas de formaciones forestales a partir del Mapa Forestal Español a escala 1:50.000. 

El Mapa Forestal Español a escala 1:50.000 ([MFE50](http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/informacion-disponible/mfe50.aspx)) está disponible para el público en la web del Ministerio de Agricultura, Alimentación y Medio Ambiente del Gobierno de España.

### Objetivo 
Obtener shapefiles del Mapa Forestal Español y generar un shapefile de la formación forestal deseada

### Motivación
Muchos investigadores tienen necesidad de trabajar con información espacial de formaciones forestales. Aunque la política de datos del Mapa Forestal de España es la descarga libre y gratuita, la forma en la que esta se ofrece así como los formatos podrían mejorar mucho. 

Para un proyecto necesitábamos la distribución de los melojares (*Quercus pyrenaica*) en la Península Ibérica. El MFE50 ofrece servicios de mapas interoperables ([WMS](http://www.magrama.gob.es/es/cartografia-y-sig/ide/directorio_datos_servicios/biodiversidad/wms_bdn_mfe_indice.aspx)). Estos servicios sirven para operaciones de visualización de información, pero no permiten trabajar con la información (realizar algún tipo de operación con Sistemas de Información Geográfica). 

Tras varias consultas al organismo solicitando la información vectorial de la formación forestal, la respuesta (tardía), entre otras cosas, decía: *ya que usted dispone de herramientas SIG, le recomiendo que se descargue la información (los shapefiles por provincias) y que seleccione fácilemtne las formaciones arboladas en función de los atributos en la base de datos* 

Es por ello que tras emplear un tiempo en conseguir el shapefile de la formación forestal, decidí que ese tiempo (pagado con dinero público) debería estar accesible a otros investigadores y al público en general, que necesite obtener información del MFE50. Ya que si un usario deseea obtener una formación forestal (el shapefile de una formación forestal) tiene que descargarse todo el mapa forestal (por provincias) y hacer consultas en cada shapefile por la formación deseada. Con los scripts que ahora se comparten los usuarios pueden generar el shapefile de forma automática. 

### Contenidos
Este repositorio consta de la siguiente información: 
* Un [script](https://gitlab.com/iecolab/mapaforestal50/blob/master/Download_shapefiles_MFE50.md) para descargar los shapefiles de la web del Mapa Forestal Español (MFE50)
* Un [csv](https://gitlab.com/iecolab/mapaforestal50/blob/master/data/url_mfe50.csv) con las urls de los archivos comprimidos que contienen los shapefiles provinciales 
* Un [script](https://gitlab.com/iecolab/mapaforestal50/blob/aeed726196d61a855e12a31962073919521a276f/Create_Formation_Distribution_Map.md) para generar un shapefile de la formación forestal deseada 

### Autor
Antonio J. Pérez-Luque
* [@ajpelu](https://twitter.com/ajpelu)
* [ajperez@ugr.es](mailto:ajperez@ugr.es)

License: Creative Commons Attribution 4.0 International License
![http://creativecommons.org/licenses/by/4.0/](https://i.creativecommons.org/l/by/4.0/88x31.png)
