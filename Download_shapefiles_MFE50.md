Este script sirve para descargarse la información espacial del Mapa Forestal Español a escala 1:50.000 ([MFE50](http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/informacion-disponible/mfe50.aspx)). Esta información está disponible para el público en la web del Ministerio de Agricultura, Alimentación y Medio Ambiente del Gobierno de España.

La información espacial, además de en formato WMS, se ofrece en formato vectorial (*shapefiles*) por provincias. En concreto se ofrecen archivos comprimidos con la información vectorial y los metadatos asociados. Para poder obtener la distribución de una formación forestal en la Península Ibérica es necesario descargarse todos los archivos vectoriales.

Este primer script sirve para poder descargar todos los shapefiles. Antes de ello es necesario tener un archivo de texto con las urls de los archivos comprimidos. Este paso ha sido imposible de automatizar ya que las urls no siguen un patron, por lo tanto es necesario obtener todas las urls. En la siguiente tabla se adjunta dichas urls.

| provincia              | url                                                                                                    |
|------------------------|--------------------------------------------------------------------------------------------------------|
| ALAVA                  | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_01_tcm7-261537.zip> |
| ALBACETE               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_02_tcm7-261538.zip> |
| ALICANTE               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_03_tcm7-261539.zip> |
| ALMERIA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_04_tcm7-261541.zip> |
| AVILA                  | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_05_tcm7-261543.zip> |
| BADAJOZ                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_06_tcm7-261544.zip> |
| ILLES BALEARS          | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_07_tcm7-261545.zip> |
| BARCELONA              | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_08_tcm7-261546.zip> |
| BURGOS                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_09_tcm7-261547.zip> |
| CACERES                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_10_tcm7-261548.zip> |
| CADIZ                  | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_11_tcm7-261551.zip> |
| CASTELLON              | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_12_tcm7-261552.zip> |
| CIUDAD REAL            | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_13_tcm7-261553.zip> |
| CORDOBA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_14_tcm7-261554.zip> |
| CORUNA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_15_tcm7-261555.zip> |
| CUENCA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_16_tcm7-261556.zip> |
| GIRONA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_17_tcm7-261557.zip> |
| GRANADA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_18_tcm7-261558.zip> |
| GUADALAJARA            | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_19_tcm7-261559.zip> |
| GIPUZKOA               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_20_tcm7-261560.zip> |
| HUELVA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_21_tcm7-261569.zip> |
| HUESCA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_22_tcm7-261571.zip> |
| JAEN                   | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_23_tcm7-261572.zip> |
| LEON                   | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_24_tcm7-261573.zip> |
| LLEIDA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_25_tcm7-261574.zip> |
| LA RIOJA               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_26_tcm7-261575.zip> |
| LUGO                   | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_27_tcm7-261576.zip> |
| MADRID                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_28_tcm7-261577.zip> |
| MALAGA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_29_tcm7-261578.zip> |
| MURCIA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_30_tcm7-261581.zip> |
| NAVARRA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_31_tcm7-261582.zip> |
| OURENSE                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_32_tcm7-261583.zip> |
| ASTURIAS               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_33_tcm7-261585.zip> |
| PALENCIA               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_34_tcm7-261586.zip> |
| LAS PALMAS             | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_35_tcm7-261588.zip> |
| PONTEVEDRA             | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_36_tcm7-261590.zip> |
| SALAMANCA              | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_37_tcm7-261592.zip> |
| SANTA CRUZ DE TENERIFE | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_38_tcm7-261593.zip> |
| CANTABRIA              | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_39_tcm7-261595.zip> |
| SEGOVIA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_40_tcm7-261598.zip> |
| SEVILLA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_41_tcm7-261602.zip> |
| SORIA                  | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_42_tcm7-261603.zip> |
| TARRAGONA              | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_43_tcm7-261604.zip> |
| TERUEL                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_44_tcm7-261605.zip> |
| TOLEDO                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_45_tcm7-261606.zip> |
| VALENCIA               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_46_tcm7-261608.zip> |
| VALLADOLID             | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_47_tcm7-261609.zip> |
| BIZKAIA                | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_48_tcm7-261610.zip> |
| ZAMORA                 | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_49_tcm7-261611.zip> |
| ZARAGOZA               | <http://www.magrama.gob.es/es/biodiversidad/servicios/banco-datos-naturaleza/MFE50_50_tcm7-261612.zip> |

La anterior tabla la guardamos como archivo csv `url_mfe50.csv`. A continuación establecemos el directorio y leemos la tabla de las urls.

``` r
# Set working directory 
di <- '/Users/ajpelu/myrepos/mapaforestal50'
setwd(di)

# Read file with urls 
myurls <- read.csv(paste(di, '/data/url_mfe50.csv', sep=''), header=TRUE, sep=';')
```

Seguidamente creamos un bucle para descargar los archivos comprimidos, descomprimirlos en un directorio determinado y eliminar los archivos comprimidos. Para el nombre de los archivos de destino hemos utilizado un patron: `mfe_xx.zip`, siendo `xx` el código de la provincia.

``` r
# Loop to download the Spanish Forest Map (MFE) by provinces 
## download and extract the zip file
## remove the zip file 
for (i in 1:nrow(myurls)){
  # Create a destination filename: MFE_XX.zip. We extract the filename from url 
  dest_file <- paste(substr(myurls[i,'url_mf50'], start=77, stop=84), '.zip', sep='')
  download.file(paste(myurls[i,'url_mf50']), destfile = dest_file)
  unzip(dest_file, exdir=paste(di, '/data/shapefiles/', sep=''))
  file.remove(dest_file)
}
```
